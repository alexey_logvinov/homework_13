﻿#include <iostream>
#include <string>
#include <iomanip>
#include <stdint.h>

int main()
{
    
    std::cout << "Enter string: ";
    std::string str;
    std::cin >> str;
    
    
    
    std::cout << "Length string: " << str.length() << "\n";
    std::cout << "First symbol: " << str[0] << "\n";
    std::cout << "Last symbol: " << str[str.size() - 1];

}

